/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.controllers;

import com.pyme2b.mitienda.models.Cliente;
import com.pyme2b.mitienda.models.MsgReturnJson;
import com.pyme2b.mitienda.services.ClientService;
import com.pyme2b.mitienda.services.UtilsManager;
import java.util.ArrayList;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author corteda
 */
@Path("cliente")
public class ClientResource {
    private UtilsManager utils;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Client
     */
    public ClientResource() {
        utils = new UtilsManager();
    }

    /**
     * Retrieves representation of an instance of com.pyme2b.mitienda.Client
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getClientes() {
        MsgReturnJson infoError;
        try{
            ClientService clientInfo = new ClientService();
            ArrayList<Cliente> data = clientInfo.listClientes();
            if (data==null){
                infoError = new MsgReturnJson(clientInfo.getMsgError(),500);
                return utils.transformObjectToJson(infoError);
            }else
                return utils.transformObjectToJson(data);
        }catch (Exception ex){
            infoError = new MsgReturnJson(ex.getMessage(),500);
            try{
                return utils.transformObjectToJson(infoError);
            }catch(Exception exinfo){
                return exinfo.getMessage();
            }
        }
    }

    @GET
    @Path("/{eanCliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public String buscaCliente(@PathParam("eanCliente")String ean)
    {
        MsgReturnJson infoError;
        try{
            ClientService clientInfo = new ClientService();
            Cliente  data = clientInfo.getCliente(ean);
            if (data==null){
                infoError = new MsgReturnJson(clientInfo.getMsgError(),500);
                return utils.transformObjectToJson(infoError);
            }else
                if ((data.getId_ciudad()==0) && (data.getId_tienda()==0)){
                    //No encontro el registro
                    return utils.transformObjectToJson(new MsgReturnJson("Registro no encontrado",200));
                }else
                    return utils.transformObjectToJson(data);
        }catch (Exception ex){
            infoError = new MsgReturnJson(ex.getMessage(),500);
            try{
                return utils.transformObjectToJson(infoError);
            }catch(Exception exinfo){
                return exinfo.getMessage();
            }
        }
    }
    
    
    /**
     * POST method for updating or creating an instance of Client
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String insertCliente(@Context UriInfo uriInfo,String jsonBody){ 
        MsgReturnJson infoMsg;
        try{
            Cliente infoCliente = utils.fromJsonToCliente(jsonBody);
            ClientService clientInfo = new ClientService();
            infoMsg = clientInfo.insertCliente(infoCliente);
            return utils.transformObjectToJson(infoMsg);
        }catch (Exception ex){
            infoMsg = new MsgReturnJson(ex.getMessage(),500);
            try{
                return utils.transformObjectToJson(infoMsg);
            }catch(Exception exinfo){
                return exinfo.getMessage();
            }
        }
    }

    /**
     * PUT method for updating or creating an instance of Client
     * @param content representation for the resource
     */
    @PUT
    @Path("/{eanCliente}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String modificarCliente(@PathParam("eanCliente")String ean,@Context UriInfo uriInfo,String jsonBody){ 
        MsgReturnJson infoMsg;
        try{
            Cliente infoCliente = utils.fromJsonToCliente(jsonBody);
            ClientService clientInfo = new ClientService();
            infoMsg = clientInfo.updateClient(ean,infoCliente);
            return utils.transformObjectToJson(infoMsg);
        }catch (Exception ex){
            infoMsg = new MsgReturnJson(ex.getMessage(),500);
            try{
                return utils.transformObjectToJson(infoMsg);
            }catch(Exception exinfo){
                return exinfo.getMessage();
            }
        }
    }
    
    /**
     * DELETE method for Delete an instance of Client
     * @param content representation for the resource
     */
    @DELETE
    @Path("/{eanCliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public String borrarCliente(@PathParam("eanCliente")String ean){ 
        MsgReturnJson infoMsg;
        try{
            ClientService clientInfo = new ClientService();
            infoMsg = clientInfo.deleteClient(ean);
            return utils.transformObjectToJson(infoMsg);
        }catch (Exception ex){
            infoMsg = new MsgReturnJson(ex.getMessage(),500);
            try{
                return utils.transformObjectToJson(infoMsg);
            }catch(Exception exinfo){
                return exinfo.getMessage();
            }
        }
    }
}
