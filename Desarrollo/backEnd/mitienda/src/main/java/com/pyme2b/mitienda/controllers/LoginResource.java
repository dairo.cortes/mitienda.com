/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.controllers;

import com.pyme2b.mitienda.models.Credenciales;
import com.pyme2b.mitienda.models.LoginBody;
import com.pyme2b.mitienda.models.MsgReturnJson;
import com.pyme2b.mitienda.services.LoginService;
import com.pyme2b.mitienda.services.UtilsManager;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author corteda
 */
@Path("login")
public class LoginResource {
    private UtilsManager utils;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of LoginResource
     */
    public LoginResource() {
        utils = new UtilsManager();
    }

    /**
     * Retrieves representation of an instance of com.pyme2b.mitienda.LoginResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers() {
        MsgReturnJson infoError;
        try{
            LoginService loginInfo = new LoginService();
            ArrayList<Credenciales> data = loginInfo.listUsers();
            if (data==null){
                infoError = new MsgReturnJson(loginInfo.getMsgError(),500);
                return utils.transformObjectToJson(infoError);
            }else
                return utils.transformObjectToJson(data);
        }catch (Exception ex){
            infoError = new MsgReturnJson(ex.getMessage(),500);
            try{
                return utils.transformObjectToJson(infoError);
            }catch(Exception exinfo){
                return exinfo.getMessage();
            }
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String postUser(@Context UriInfo uriInfo,String jsonBody){
        MsgReturnJson infoMsg;
        try{
            LoginBody infoLogin = utils.fromJsonToLoginBody(jsonBody);
            LoginService loginInfo = new LoginService();
            Credenciales userData = loginInfo.validateLogin(infoLogin);
            if (userData==null){
                infoMsg = new MsgReturnJson(loginInfo.getMsgError(),500);
                return utils.transformObjectToJson(infoMsg);
            }
            return utils.transformObjectToJson(userData);
        }catch (Exception ex){
            infoMsg = new MsgReturnJson(ex.getMessage(),500);
            try{
                return utils.transformObjectToJson(infoMsg);
            }catch(Exception exinfo){
                return exinfo.getMessage();
            }
        }
        
    }

}
