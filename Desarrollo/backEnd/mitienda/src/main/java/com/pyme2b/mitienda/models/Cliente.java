/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;

/**
 *
 * @author corteda
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cliente {
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;
    private int id_ciudad;
    private int id_tienda;
    private String email;
    private String ean;
    private String perfil;
    private Date fecha_creacion;
    private short esta_borrado;
    

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
         
    public String getNombre(){
            return nombre;
    }

    public void setNombre(String nombre){
            this.nombre=nombre;
    }

    public String getApellido(){
            return apellido;
    }

    public void setApellido(String apellido){
            this.apellido=apellido;
    }

    public String getDireccion(){
            return direccion;
    }

    public void setDireccion(String direccion){
            this.direccion=direccion;
    }

    public String getTelefono(){
            return telefono;
    }

    public void setTelefono(String telefono){
            this.telefono=telefono;
    }

    public int getId_ciudad(){
            return id_ciudad;
    }

    public void setId_ciudad(int id_ciudad){
            this.id_ciudad=id_ciudad;
    }

    public int getId_tienda(){
            return id_tienda;
    }

    public void setId_tienda(int id_tienda){
            this.id_tienda=id_tienda;
    }

    public String getEmail(){
            return email;
    }

    public void setEmail(String email){
            this.email=email;
    }

    public String getEan(){
            return ean;
    }

    public void setEan(String ean){
            this.ean=ean;
    }

    public String getPerfil(){
            return perfil;
    }

    public void setPerfil(String perfil){
            this.perfil=perfil;
    }

    public Cliente() {
    }

    public short getEsta_borrado() {
        return esta_borrado;
    }

    public void setEsta_borrado(short esta_borrado) {
        this.esta_borrado = esta_borrado;
    }

    public Cliente(String nombre, String apellido, String direccion, String telefono, int id_ciudad, int id_tienda, String email, String ean, String perfil, Date fecha_creacion, short esta_borrado) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.id_ciudad = id_ciudad;
        this.id_tienda = id_tienda;
        this.email = email;
        this.ean = ean;
        this.perfil = perfil;
        this.fecha_creacion = fecha_creacion;
        this.esta_borrado = esta_borrado;
    }

    @Override
    public String toString() {
        return "Cliente{" + "nombre=" + nombre + ", apellido=" + apellido + ", direccion=" + direccion + ", telefono=" + telefono + ", id_ciudad=" + id_ciudad + ", id_tienda=" + id_tienda + ", email=" + email + ", ean=" + ean + ", perfil=" + perfil + ", fecha_creacion=" + fecha_creacion + ", esta_borrado=" + esta_borrado + '}';
    }

    
    
    
    
}