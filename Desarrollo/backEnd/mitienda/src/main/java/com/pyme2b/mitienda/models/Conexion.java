/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.models;

/**
 *
 * @author corteda
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
    private Connection con;
    private static final String DB_DRIVER="com.mysql.cj.jdbc.Driver";
    private static final String HOST="localhost:3306";
    private static final String DB = "mitienda";
    private static final String URL = "jdbc:mysql://" + HOST + "/" + DB + "?useSSL=false";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "bdDaicorga2022";
    private String _msgError;


    public Conexion(){
            con=null;
            _msgError = "";
    }
       
    
        
    public boolean createCnx()
    {
        try{
            if (con==null){
                    Class.forName(DB_DRIVER);
                    con = DriverManager.getConnection(URL,USERNAME,PASSWORD);
                }
            return true;
        }catch (Exception e){
            setMsgError(e.getMessage());
            return false;
        }
    }
    
    
    public ResultSet queryBD(String queryData) {
        try {
            if (con==null){
                if (!createCnx())
                    return null;
            }
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(queryData);
            return rs;
        } catch (SQLException sqlex) {
            setMsgError(sqlex.getMessage());
            return null;
        } catch (RuntimeException rex) {
            setMsgError(rex.getMessage());
            return null;
        } catch (Exception ex) {
            setMsgError(ex.getMessage());
            return null;
        }
    }
    
    public boolean executeQueryBD(String queryData) {
        try {
            if (con==null){
                if (!createCnx())
                    return false;
            }
            Statement stmt = con.createStatement();
            stmt.execute(queryData);
            return true;
        } catch (SQLException | RuntimeException sqlex) {
            setMsgError(sqlex.getMessage());
            return false;
        }
    }

    public void CloseBD()
    {
        try{
            if (con!=null){
                con.close();
                con = null;
            }
        }catch (Exception e){
            setMsgError(e.getMessage());
        }
    }                

    /**
     * @return the _msgError
     */
    public String getMsgError() {
        return _msgError;
    }

    /**
     * @param _msgError the _msgError to set
     */
    public void setMsgError(String _msgError) {
        this._msgError = _msgError;
    }

    
}
