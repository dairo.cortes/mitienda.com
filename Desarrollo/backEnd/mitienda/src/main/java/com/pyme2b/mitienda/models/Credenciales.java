/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.models;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 *
 * @author corteda
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class Credenciales {
    private String usuario;
    private String clave;
    private String id_rol;
    private int id_cliente;
    private Date fecha_cambio_clave;
    private Date fecha_creacion;

    public Date getFecha_cambio_clave() {
        return fecha_cambio_clave;
    }

    public void setFecha_cambio_clave(Date fecha_cambio_clave) {
        this.fecha_cambio_clave = fecha_cambio_clave;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    public String getUsuario(){
            return usuario;
    }

    public void setUsuario(String usuario){
            this.usuario=usuario;
    }

    public String getClave(){
            return clave;
    }

    public void setClave(String clave){
            this.clave=clave;
    }

    public String getId_rol(){
            return id_rol;
    }

    public void setId_rol(String id_rol){
            this.id_rol=id_rol;
    }

    public int getId_cliente(){
            return id_cliente;
    }

    public void setId_cliente(int id_cliente){
            this.id_cliente=id_cliente;
    }

    public Credenciales() {
    }

    public Credenciales(String usuario, String clave, String id_rol, int id_cliente, Date fecha_cambio_clave, Date fecha_creacion) {
        this.usuario = usuario;
        this.clave = clave;
        this.id_rol = id_rol;
        this.id_cliente = id_cliente;
        this.fecha_cambio_clave = fecha_cambio_clave;
        this.fecha_creacion = fecha_creacion;
    }

    @Override
    public String toString() {
        return "Credenciales{" + "usuario=" + usuario + ", clave=" + clave + ", id_rol=" + id_rol + ", id_cliente=" + id_cliente + ", fecha_cambio_clave=" + fecha_cambio_clave + ", fecha_creacion=" + fecha_creacion + '}';
    }
       
    
        
        
}