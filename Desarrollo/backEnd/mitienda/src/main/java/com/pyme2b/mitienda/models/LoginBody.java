/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author corteda
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginBody {
    private String user;
    private String password;

    public LoginBody() {
    }

    public LoginBody(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginBody{" + "_user=" + user + ", _password=" + password + '}';
    }
    
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
