/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.models;

import java.util.Date;

/**
 *
 * @author corteda
 */
public class MsgReturnJson {
    private String message;
    private int errorCode;
    private long timeStamp;

    public long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String toString() {
        return "MsgReturnJson{" + "message=" + message + ", errorCode=" + errorCode + ", timeStamp=" + timeStamp + '}';
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int code) {
        this.errorCode = code;
    }

    public MsgReturnJson(String message, int code) {
        this.message = message;
        this.errorCode = code;
        this.timeStamp = (new Date()).getTime();
    }

    public MsgReturnJson() {
    }

   
    
    
    
    
}
