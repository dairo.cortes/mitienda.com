/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.services;

import com.pyme2b.mitienda.models.Cliente;
import com.pyme2b.mitienda.models.Conexion;
import com.pyme2b.mitienda.models.MsgReturnJson;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author corteda
 */
public class ClientService {
    private String _msgError;

    public String getMsgError() {
        return _msgError;
    }

    public void setMsgError(String _msgError) {
        this._msgError = _msgError;
    }
    private Conexion myBD;

    public ClientService() {
        _msgError = "";
        myBD = new Conexion();
    }
    
    private ArrayList generateData(ResultSet data){
        ArrayList<Cliente> listaClientes = new ArrayList<>();
        try{
            while (data.next()){
                Cliente cliente = new Cliente();
                cliente.setApellido(data.getString("apellido"));
                cliente.setDireccion(data.getString("direccion"));
                cliente.setEan(data.getString("ean"));
                cliente.setEmail(data.getString("email"));
                Timestamp timestamp = data.getTimestamp("fecha_creacion");
                if (timestamp != null)
                    cliente.setFecha_creacion(new Date(timestamp.getTime()));
                cliente.setId_ciudad(data.getInt("id_ciudad"));
                cliente.setId_tienda(data.getInt("id_tienda"));
                cliente.setNombre(data.getString("nombre"));
                cliente.setPerfil(data.getString("perfil"));
                cliente.setTelefono(data.getString("telefono"));
                listaClientes.add(cliente);
            }
            return listaClientes;
        }catch (Exception ex){
            _msgError = ex.getMessage();
            return null;
        }
        
    }
    
    public ArrayList listClientes()
    {
        return queryClient("WHERE esta_borrado=0");
    }
    
    public Cliente getCliente(String ean){
        String cmd = String.format(" WHERE ean='%s' and esta_borrado=0", ean);
        ArrayList<Cliente> infoClientes = queryClient(cmd);
        if (infoClientes.size()>0){
            return infoClientes.get(0);
        }
        if (infoClientes.size()==0)
            return new Cliente();
        else
            return null;
    }
    
    private ArrayList queryClient(String where) {
        ArrayList<Cliente> listaClientes;
        try{
            String cmd = "SELECT id_cliente, nombre, apellido, direccion, telefono, id_ciudad, id_tienda, email, fecha_creacion, ean, perfil FROM mitienda.clientes " + where;
            if (myBD==null)
                myBD.createCnx();
            ResultSet rtaBD = myBD.queryBD(cmd);
            if (rtaBD==null){
                this._msgError=myBD.getMsgError();
                return null;
            }
            return(generateData(rtaBD));
        }catch(Exception ex){
            this._msgError = ex.getMessage();
            return null;
        }
    }
    
    public MsgReturnJson insertCliente(Cliente infoCliente){
        MsgReturnJson msg;
        try{
            String cmd = String.format("INSERT INTO mitienda.clientes (nombre, apellido, direccion, telefono, id_ciudad, id_tienda, email, fecha_creacion, ean, perfil) " +
                                       "VALUES('%s', '%s', '%s', '%s', %d, %d, '%s', Now(), '%s', '%s');",infoCliente.getNombre(),infoCliente.getApellido(),infoCliente.getDireccion(), 
                                       infoCliente.getTelefono(),infoCliente.getId_ciudad(),infoCliente.getId_tienda(),infoCliente.getEmail(),infoCliente.getEan(),infoCliente.getPerfil());
            if (myBD==null)
                myBD.createCnx();
            myBD.executeQueryBD(cmd);
            msg = new MsgReturnJson("Cliente creado",200);
            return msg;
        }catch(Exception ex){
            msg = new MsgReturnJson(ex.getMessage(),500);
            return msg;
        }
    }

    public MsgReturnJson updateClient(String ean,Cliente infoCliente){
        MsgReturnJson msg;
        try{
            String cmd = String.format("UPDATE mitienda.clientes set nombre='%s', apellido='%s', direccion='%s', telefono='%s', id_ciudad=%d, id_tienda=%d, email='%s', fecha_creacion=Now(), perfil='%s' " +
                                       "WHERE ean='%s'",infoCliente.getNombre(),infoCliente.getApellido(),infoCliente.getDireccion(), 
                                       infoCliente.getTelefono(),infoCliente.getId_ciudad(),infoCliente.getId_tienda(),infoCliente.getEmail(),infoCliente.getPerfil(),infoCliente.getEan());
            if (myBD==null)
                myBD.createCnx();
            myBD.executeQueryBD(cmd);
            msg = new MsgReturnJson("Cliente modificado",200);
            return msg;
        }catch(Exception ex){
            msg = new MsgReturnJson(ex.getMessage(),500);
            return msg;
        }
    }
    
    public MsgReturnJson deleteClient(String ean){
        MsgReturnJson msg;
        try{
            String cmd1 = String.format("UPDATE clientes SET esta_borrado=1 WHERE ean='%s'",ean);
            String cmd2 = String.format("UPDATE credenciales SET esta_borrado=1 WHERE id_cliente=(SELECT id_cliente FROM clientes WHERE ean='%s')",ean);
            if (myBD==null)
                myBD.createCnx();
            myBD.executeQueryBD(cmd2);
            myBD.executeQueryBD(cmd1);
            msg = new MsgReturnJson("Cliente Borrado",200);
            return msg;
        }catch(Exception ex){
            msg = new MsgReturnJson(ex.getMessage(),500);
            return msg;
        }
    }
}
