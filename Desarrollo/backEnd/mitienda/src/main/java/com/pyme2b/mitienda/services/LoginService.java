/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.services;

import com.pyme2b.mitienda.models.Conexion;
import com.pyme2b.mitienda.models.Credenciales;
import com.pyme2b.mitienda.models.LoginBody;
import com.pyme2b.mitienda.models.MsgReturnJson;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author corteda
 */


public class LoginService {
    private String _msgError;

    public String getMsgError() {
        return _msgError;
    }

    public void setMsgError(String _msgError) {
        this._msgError = _msgError;
    }
    private Conexion myBD;
    
    public LoginService() {
        _msgError = "";
        myBD = new Conexion();
    }
    
    public Credenciales validateLogin(LoginBody infoBody)
    {
        MsgReturnJson msg;
        try{
            String cmd = String.format("SELECT id_usuario, usuario, clave, id_rol, fecha_cambio_clave, fecha_creacion, id_cliente FROM mitienda.credenciales WHERE usuario='%s' AND clave='%s' AND esta_borrado=0",infoBody.getUser(),infoBody.getPassword());
            if (myBD==null)
                myBD.createCnx();
            ResultSet rtaBD = myBD.queryBD(cmd);
            if (rtaBD==null){
                this._msgError =myBD.getMsgError();
                return null;
            }
            ArrayList<Credenciales> listaUsuario = generateData(rtaBD);
            if (listaUsuario.size()>0){
                Credenciales dataLogin = (Credenciales)listaUsuario.get(0);
                return dataLogin;
            }else{
                this._msgError="Usuario o password desconocido";
                return null;
            }         
        }catch(Exception ex){
            this._msgError=ex.getMessage();
            return null;
        }
    }  
    
    private ArrayList generateData(ResultSet data){
        ArrayList<Credenciales> listaUsuario = new ArrayList<>();
        try{
            while (data.next()){
                Credenciales credenciales = new Credenciales();
                credenciales.setClave(data.getString("clave"));
                credenciales.setId_cliente(data.getInt("id_cliente"));
                credenciales.setId_rol(data.getString("id_rol"));
                credenciales.setUsuario(data.getString("usuario"));
                Timestamp timestamp = data.getTimestamp("fecha_cambio_clave");
                if (timestamp != null)
                    credenciales.setFecha_cambio_clave(new Date(timestamp.getTime()));
                timestamp = data.getTimestamp("fecha_creacion");
                if (timestamp != null)
                    credenciales.setFecha_creacion(new Date(timestamp.getTime()));
                listaUsuario.add(credenciales);
            }
            return listaUsuario;
        }catch (Exception ex){
            _msgError = ex.getMessage();
            return null;
        }
        
    }
    
    public ArrayList listUsers() {
        ArrayList<Credenciales> listaUsuario;
        try{
            String cmd = "SELECT id_usuario, usuario, clave, id_rol, fecha_cambio_clave, fecha_creacion, id_cliente FROM mitienda.credenciales WHERE esta_borrado=0";
            if (myBD==null)
                myBD.createCnx();
            ResultSet rtaBD = myBD.queryBD(cmd);
            if (rtaBD==null){
                this._msgError=myBD.getMsgError();
                return null;
            }
            return(generateData(rtaBD));
        }catch(Exception ex){
            _msgError = ex.getMessage();
            return null;
        }
    }
   
}
