/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pyme2b.mitienda.services;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pyme2b.mitienda.models.Cliente;
import com.pyme2b.mitienda.models.LoginBody;
import java.io.IOException;
import java.time.Instant;
import java.util.Base64;
import java.util.UUID;
/**
 *
 * @author corteda
 */
public class UtilsManager {

    public UtilsManager() {
    }
    
    
    
    public String toBase64Decode(String content) throws Exception {
        try {
            byte[] decodedBytes = Base64.getDecoder().decode((content != null) ? content : "");
            return new String(decodedBytes);
        } catch (IllegalArgumentException ex) {
            throw new Exception("No fue posible decodificar el contenido del archivo", ex);
        }
    }
    
    public LoginBody fromJsonToLoginBody(String jsonString) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(jsonString, LoginBody.class);
        } catch (IOException ex) {
            throw new Exception("Falló la conversión de json a LoginBody", ex);
        }
    }
    
    
    public Cliente fromJsonToCliente(String jsonString) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(jsonString, Cliente.class);
        } catch (Exception ex) {
            throw new Exception("Falló la conversión de json a Cliente", ex);
        }
    }
    
    public String toBase64String(String content) {
        String encoded = "";
        if (content == null || content.length() == 0) {
            return encoded;
        } else {
            encoded = Base64.getEncoder().encodeToString(content.getBytes());
        }
        return encoded;
    }
    
    public String transformObjectToJson(Object object) throws Exception {
        String jsonString = "";
        if (object == null) {
            throw new Exception("Falló la transformación a json por que el objeto es nulo.");
        }
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setSerializationInclusion(Include.NON_NULL);
            objectMapper.setSerializationInclusion(Include.NON_EMPTY);
            jsonString = objectMapper.writeValueAsString(object).replace("{},", "");
        } catch (JsonProcessingException ex) {
            throw new Exception(ex.getMessage());
        }
        return jsonString;

    }
    
}
