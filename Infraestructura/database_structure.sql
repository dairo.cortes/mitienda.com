CREATE DATABASE `mitienda` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

-- mitienda.ciudades definition

CREATE TABLE `ciudades` (
  `id_ciudad` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `departamento` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_ciudad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.roles definition

CREATE TABLE `roles` (
  `id_rol` varchar(15) NOT NULL,
  `nombre_funcionalidad` varchar(30) NOT NULL,
  `clase` varchar(100) NOT NULL,
  `descripcion` varchar(120) DEFAULT NULL,
  KEY `roles_id_rol_IDX` (`id_rol`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.proveedores definition

CREATE TABLE `proveedores` (
  `id_proveedor` int NOT NULL AUTO_INCREMENT,
  `nombre_empresa` varchar(60) NOT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `id_ciudad` int NOT NULL,
  `contacto` varchar(60) DEFAULT NULL,
  `email` varchar(60) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  PRIMARY KEY (`id_proveedor`),
  KEY `id_ciudad` (`id_ciudad`),
  CONSTRAINT `proveedores_id_ciudad` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id_ciudad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.tiendas definition

CREATE TABLE `tiendas` (
  `id_tienda` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `id_ciudad` int NOT NULL,
  `contacto` varchar(50) DEFAULT NULL,
  `email` varchar(60) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `id_logo` int DEFAULT NULL,
  PRIMARY KEY (`id_tienda`),
  KEY `id_ciudad` (`id_ciudad`),
  CONSTRAINT `tiendas_ibfk_1` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id_ciudad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.categorias definition

CREATE TABLE `categorias` (
  `id_categoria` int NOT NULL AUTO_INCREMENT,
  `id_tienda` int NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `descripcion` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  PRIMARY KEY (`id_categoria`),
  UNIQUE KEY `categorias_id_tienda_IDX` (`id_tienda`,`codigo`) USING BTREE,
  KEY `id_tienda` (`id_tienda`),
  CONSTRAINT `categoria_id_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.clientes definition

CREATE TABLE `clientes` (
  `id_cliente` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `id_ciudad` int NOT NULL,
  `id_tienda` int NOT NULL,
  `email` varchar(60) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `ean` varchar(30) DEFAULT NULL,
  `perfil` varchar(3) NOT NULL COMMENT 'Puede ser TND=Tendero o CMP=Comprador',
  PRIMARY KEY (`id_cliente`),
  KEY `id_tienda` (`id_tienda`),
  KEY `id_ciudad` (`id_ciudad`),
  CONSTRAINT `clientes_id_ciudad` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id_ciudad`),
  CONSTRAINT `clientes_id_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.credenciales definition

CREATE TABLE `credenciales` (
  `id_usuario` int NOT NULL AUTO_INCREMENT,
  `usuario` varchar(60) NOT NULL,
  `clave` varchar(60) NOT NULL,
  `id_rol` varchar(15) NOT NULL,
  `fecha_cambio_clave` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `id_cliente` int NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `credenciales_usuario_IDX` (`usuario`) USING BTREE,
  KEY `id_rol` (`id_rol`),
  KEY `credenciales_FK` (`id_cliente`),
  CONSTRAINT `credenciales_FK` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `credenciales_id_rol` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.orden_compra definition

CREATE TABLE `orden_compra` (
  `id_orden_compra` int NOT NULL AUTO_INCREMENT,
  `id_tienda` int NOT NULL,
  `id_cliente` int NOT NULL,
  `fecha_oc` datetime NOT NULL,
  `fecha_entrega` datetime DEFAULT NULL,
  `numero_oc` varchar(35) NOT NULL,
  `total_impuestos` float DEFAULT NULL,
  `total_descuentos` float DEFAULT NULL,
  `total_precio_oc` float NOT NULL,
  `descripcion` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id_orden_compra`),
  KEY `id_tienda` (`id_tienda`),
  KEY `id_cliente` (`id_cliente`),
  CONSTRAINT `orden_compra_id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`),
  CONSTRAINT `orden_compra_id_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.productos definition

CREATE TABLE `productos` (
  `id_producto` int NOT NULL AUTO_INCREMENT,
  `sku` varchar(35) DEFAULT NULL,
  `descricion_larga` varchar(120) NOT NULL,
  `descripcion_corta` varchar(30) NOT NULL,
  `tiene_foto` tinyint(1) NOT NULL,
  `precio_unitario` float DEFAULT NULL,
  `precio_total` float DEFAULT NULL,
  `paquete` int NOT NULL,
  `id_categoria` int NOT NULL,
  `id_tienda` int NOT NULL,
  `id_proveedor` int NOT NULL,
  `fecha_creacion` timestamp NOT NULL,
  `esta_borrado` tinyint(1) NOT NULL,
  `cantidad_disponible` int NOT NULL,
  `ean` varchar(25) NOT NULL,
  `tiene_descuentos` tinyint(1) NOT NULL,
  `tiene_caracteristicas` tinyint(1) NOT NULL,
  `tiene_impuestos` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `id_tienda` (`id_tienda`),
  KEY `id_categoria` (`id_categoria`),
  KEY `id_proveedor` (`id_proveedor`),
  KEY `productos_id_tienda_IDX` (`id_tienda`,`ean`,`fecha_creacion`) USING BTREE,
  CONSTRAINT `productos_id_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`),
  CONSTRAINT `productos_id_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`),
  CONSTRAINT `productos_id_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.tienda_proveedor definition

CREATE TABLE `tienda_proveedor` (
  `id_tienda` int NOT NULL,
  `id_proveedor` int NOT NULL,
  PRIMARY KEY (`id_tienda`,`id_proveedor`),
  KEY `id_tienda` (`id_tienda`),
  KEY `id_proveedor` (`id_proveedor`),
  CONSTRAINT `tienda_proveedor_id_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`),
  CONSTRAINT `tienda_proveedor_id_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.caracteristicas definition

CREATE TABLE `caracteristicas` (
  `id_producto` int NOT NULL,
  `llave` varchar(20) NOT NULL,
  `valor` varchar(60) NOT NULL,
  KEY `id_producto` (`id_producto`),
  CONSTRAINT `caracteristicas_id_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.descuentos definition

CREATE TABLE `descuentos` (
  `id_producto` int NOT NULL,
  `nombre_descuento` varchar(20) NOT NULL,
  `descto_valor` float DEFAULT NULL,
  `descto_porcentaje` decimal(10,0) DEFAULT NULL,
  KEY `id_producto` (`id_producto`),
  CONSTRAINT `descuentos_id_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.impuestos definition

CREATE TABLE `impuestos` (
  `id_producto` int NOT NULL,
  `nombre_impuesto` varchar(20) NOT NULL,
  `impto_valor` float DEFAULT NULL,
  `impto_porcentaje` decimal(10,0) DEFAULT NULL,
  KEY `id_producto` (`id_producto`),
  CONSTRAINT `impuestos_id_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.inventario definition

CREATE TABLE `inventario` (
  `id_inventario` int NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `accion` varchar(3) NOT NULL,
  `id_producto` int NOT NULL,
  `cantidad` int NOT NULL,
  `precio_unitario` float DEFAULT NULL,
  `precio_total` float DEFAULT NULL,
  `descripcion` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id_inventario`),
  KEY `id_producto` (`id_producto`),
  CONSTRAINT `inventario_id_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- mitienda.items definition

CREATE TABLE `items` (
  `id_item` int NOT NULL AUTO_INCREMENT,
  `id_producto` int NOT NULL,
  `cantidad` int NOT NULL,
  `precio_unitario` float NOT NULL,
  `precio_total` float NOT NULL,
  `impuesto_total` float DEFAULT NULL,
  `descuento_total` float DEFAULT NULL,
  `id_orden_compra` int NOT NULL,
  PRIMARY KEY (`id_item`),
  KEY `id_producto` (`id_producto`),
  KEY `id_orden_compra` (`id_orden_compra`),
  CONSTRAINT `items_id_orden_compra` FOREIGN KEY (`id_orden_compra`) REFERENCES `orden_compra` (`id_orden_compra`),
  CONSTRAINT `items_id_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;